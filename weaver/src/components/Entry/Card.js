import React, { Component } from 'react';
import { shape, string, number, node, func, bool } from 'prop-types';
import styled, { css } from 'react-emotion';

import ProfileCard from '../ProfileCard';
import Tabs from '../Tabs';

import { border, blue } from '../../colors';

const leftRightPadding = css`
  padding-left: 16px;
  padding-right: 16px;
`;

const CardWrapper = styled('div')`
  padding-top: 8px;
  padding-bottom: 8px;
  border-bottom: ${props => props.noBorder ? 'none' : `1px solid ${border}`};
`;

const activeText = css`
  color: ${blue};
`;

export default class Card extends Component {
  get tabs() {
    return [
      {
        id: 1,
        name: 'post',
        childNode: (
          <span data-testid="detailBtn" className={activeText} onClick={this.handleDetailClick}>
            Detail
          </span>
        ),
        align: 1
      }
    ];
  }

  handleDetailClick = () => {
    const { id, onDetailClick } = this.props;

    onDetailClick({ id });
  };

  render() {
    const { user, children, noBorder } = this.props;

    return (
      <CardWrapper noBorder={noBorder}>
        <ProfileCard name={user.name} id={user.id} />
        {children}
        <div className={leftRightPadding}>
          <Tabs
            tabs={this.tabs}
            activeId={1}
            noBorder
          />
        </div>
      </CardWrapper>
    );
  }
}

Card.propTypes = {
  children: node,
  id: number.isRequired,
  noBorder: bool,
  onDetailClick: func,
  user: shape({
    name: string,
    id: number,
    username: string
  }).isRequired
};

Card.defaultProps = {
  children: null,
  noBorder: false,
  onDetailClick: () => {}
};
