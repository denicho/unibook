import React, { Fragment } from 'react';
import { string } from 'prop-types';
import styled, { css } from 'react-emotion';

const leftRightPadding = css`
  padding-left: 16px;
  padding-right: 16px;
`;

const PostTitle = styled('div')`
  font-weight: 500;
  font-size: 16px;
  margin-bottom: 8px;
`;

const TextPost = ({ title, body }) => (
  <Fragment>
    <PostTitle data-testid="postTitle" className={leftRightPadding}>{title}</PostTitle>
    <div data-testid="postBody" className={leftRightPadding}>{body}</div>
  </Fragment>
);

TextPost.propTypes = {
  body: string.isRequired,
  title: string.isRequired
};

export default TextPost;
