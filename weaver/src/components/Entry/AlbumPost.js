import React, { Fragment } from 'react';
import { string } from 'prop-types';
import styled, { css } from 'react-emotion';

import Image from '../Image';

const leftRightPadding = css`
  padding-left: 16px;
  padding-right: 16px;
`;

const ImageThumbsWrapper = styled('div')`
  display: flex;
  margin-bottom: 5px;
`;

const MockImageWrapper = styled('div')`
  margin-right: 8px;
`;

const MockImage = () => (
  <MockImageWrapper>
    <Image
      rounded={false}
      src="https://via.placeholder.com/44x44"
      height="44px"
      width="44px"
    />
  </MockImageWrapper>
);

const AlbumPost = ({ title }) => (
  <Fragment>
    <ImageThumbsWrapper className={leftRightPadding}>
      <MockImage />
      <MockImage />
    </ImageThumbsWrapper>
    <div data-testid="albumTitle" className={leftRightPadding}>{title}</div>
  </Fragment>
);

AlbumPost.propTypes = {
  title: string.isRequired
};

export default AlbumPost;
