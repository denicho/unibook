import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import Card from '../Card';

const props = {
  id: 1,
  onDetailClick: jest.fn(),
  user: {
    id: 1,
    name: 'deni',
    username: 'denicho',
  }
};

const setup = () => {
  const utils = render(<Card {...props} />);
  const detailBtn = utils.getByTestId('detailBtn');

  return {
    detailBtn,
    props,
    ...utils
  };
};

describe('<Card />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup();

    expect(tree).toMatchSnapshot();
  });

  test('get id when detail clicked', () => {
    const { detailBtn } = setup();

    fireEvent.click(detailBtn);

    const firstCalls = props.onDetailClick.mock.calls[0][0];

    expect(props.onDetailClick).toHaveBeenCalled();
    expect(firstCalls.id).toBe(props.id);
  });

  test('no border', () => {
    const customProps = {
      ...props,
      noBorder: true,
    }
    const tree = render(<Card {...customProps} />);

    expect(tree).toMatchSnapshot();
  })
});
