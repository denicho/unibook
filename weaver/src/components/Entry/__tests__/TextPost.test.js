import React from 'react';
import { render, cleanup } from 'react-testing-library';
import TextPost from '../TextPost';

const props = {
  title: 'A post title',
  body: 'A post body',
};

const setup = () => {
  const utils = render(<TextPost {...props} />);
  const postTitle = utils.getByTestId('postTitle');
  const postBody = utils.getByTestId('postBody');

  return {
    postTitle,
    postBody,
    props,
    ...utils
  };
};

describe('<TextPost />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup();

    expect(tree).toMatchSnapshot();
  });

  test('title and body renders', () => {
    const { postTitle, postBody } = setup();

    expect(postTitle.innerHTML).toBe(props.title);
    expect(postBody.innerHTML).toBe(props.body);
  });
});
