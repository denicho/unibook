import React from 'react';
import { render, cleanup } from 'react-testing-library';
import AlbumPost from '../AlbumPost';

const setup = () => {
  const props = {
    title: 'An album title'
  };
  const utils = render(<AlbumPost {...props} />);
  const albumTitle = utils.getByTestId('albumTitle');

  return {
    albumTitle,
    props,
    ...utils
  };
};

describe('<AlbumPost />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup();

    expect(tree).toMatchSnapshot();
  });

  test('album title renders', () => {
    const { albumTitle, props } = setup();

    expect(albumTitle.innerHTML).toBe(props.title);
  })
});
