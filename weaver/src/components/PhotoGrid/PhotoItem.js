import React, { PureComponent } from 'react';
import { string, func, number } from 'prop-types';
import styled from 'react-emotion';

const PhotoItemWrapper = styled('img')`
  display: block;
  height: 100px;
  flex-basis: 32%;
  height: calc(100vw - 1% / 3);
  width: 32%;
  margin-top: 1%;
  margin-left: 1%;
`;

export default class PhotoItem extends PureComponent {
  handlePhotoClick = () => {
    const { id, onClick } = this.props;

    onClick({ id });
  };

  render() {
    const { src } = this.props;

    return <PhotoItemWrapper data-testid="photoItem" src={src} onClick={this.handlePhotoClick} />;
  }
}

PhotoItem.propTypes = {
  id: number.isRequired,
  onClick: func,
  src: string
};

PhotoItem.defaultProps = {
  src: '',
  onClick: () => {}
};
