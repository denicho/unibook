import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import PhotoItem from '../PhotoItem';

const setup = () => {
  const props = {
    id: 1,
    onClick: jest.fn(),
    src: 'https://via.placeholder.com/100x100'
  };
  const utils = render(<PhotoItem {...props} />);
  const photoItem = utils.getByTestId('photoItem');

  return {
    photoItem,
    props,
    ...utils
  };
};

describe('<PhotoItem />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup();

    expect(tree).toMatchSnapshot();
  });

  test('click image triggers fn', () => {
    const { photoItem, props } = setup();

    fireEvent.click(photoItem);

    expect(props.onClick).toHaveBeenCalled();
  })
});
