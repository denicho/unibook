import React from 'react';
import { func, arrayOf, shape, string, number } from 'prop-types';
import styled from 'react-emotion';

import PhotoItem from './PhotoItem';

const GridWrapper = styled('div')`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;

const PhotoGrid = ({ photos, onPhotoClick }) => (
  <GridWrapper>
    {photos.map((photo) => (
      <PhotoItem id={photo.id} key={photo.id} src={photo.thumbnailUrl} onClick={onPhotoClick} />
    ))}
  </GridWrapper>
);

PhotoGrid.propTypes = {
  onPhotoClick: func,
  photos: arrayOf(
    shape({
      id: number.isRequired,
      thumbnailUrl: string.isRequired
    })
  )
};

PhotoGrid.defaultProps = {
  photos: [],
  onPhotoClick: () => {}
};

export default PhotoGrid;
