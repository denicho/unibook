import React, { PureComponent } from 'react';
import { bool, string, func, number, oneOf, node } from 'prop-types';
import styled from 'react-emotion';

const setAlignment = (align) => {
  if (align === -1) {
    return 'flex-start';
  }

  if (align === 1) {
    return 'flex-end';
  }

  return 'center';
}

const TabItemWrapper = styled('div')`
  flex: 1;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: ${props => setAlignment(props.align)};
`;

const TabIcon = styled('img')`
  width: 24px;
  height: 24px;
`;

export default class TabItem extends PureComponent {
  handleClick = () => {
    const { onClick, id } = this.props;

    onClick({ id });
  };

  render() {
    const { id, icon, active, activeIcon, childNode, align } = this.props;

    return (
      <TabItemWrapper data-testid={`tabItem-${id}`} key={id} onClick={this.handleClick} align={align}>
        {!childNode && <TabIcon src={active ? activeIcon : icon} />}
        {childNode && childNode}
      </TabItemWrapper>
    );
  }
}

TabItem.propTypes = {
  active: bool.isRequired,
  activeIcon: string,
  align: oneOf([0, -1, 1]), // 0 for center, -1 for left, 1 for right.
  childNode: node,
  icon: string,
  id: number.isRequired,
  onClick: func,
};

TabItem.defaultProps = {
  align: 0,
  activeIcon: '',
  icon: '',
  onClick: () => {},
  childNode: null,
};
