import React from 'react';
import {
  arrayOf,
  shape,
  number,
  string,
  func,
  oneOf,
  bool,
  node
} from 'prop-types';
import styled from 'react-emotion';

import TabItem from './TabItem';
import { border } from '../../colors';

const TabsWrapper = styled('div')`
  display: flex;
  border-bottom: ${(props) =>
    props.noBorder ? 'none' : `1px solid ${border}`};
`;

const Tabs = ({ tabs, onClick, activeId, noBorder }) => (
  <TabsWrapper noBorder={noBorder}>
    {tabs.map((tab) => (
      <TabItem
        key={tab.id}
        onClick={onClick}
        id={tab.id}
        childNode={tab.childNode}
        icon={tab.icon}
        activeIcon={tab.activeIcon}
        active={tab.id === activeId}
        align={tab.align || 0}
      />
    ))}
  </TabsWrapper>
);

Tabs.propTypes = {
  activeId: number,
  noBorder: bool,
  onClick: func,
  tabs: arrayOf(
    shape({
      active: bool,
      activeIcon: string,
      align: oneOf([0, -1, 1]), // 0 for center, -1 for left, 1 for right.
      childNode: node,
      icon: string,
      id: number.isRequired,
      onClick: func
    })
  ).isRequired
};

Tabs.defaultProps = {
  onClick: () => {},
  activeId: 0,
  noBorder: false
};

export default Tabs;
