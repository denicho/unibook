import React, { PureComponent } from 'react';
import { string, func, number, bool } from 'prop-types';
import styled from 'react-emotion';

import Image from '../Image';

const ProfileCardWrapper = styled('div')`
  width: 100%;
  display: flex;
  padding: ${(props) => (props.big ? '16px' : '8px 16px')};
  border-bottom: ${(props) => (props.big ? '1px solid #DDD' : 'none')};
`;

const ProfileCardImageWrapper = styled('div')`
  width: ${(props) => (props.big ? '72px' : '44px')};
  height: ${(props) => (props.big ? '72px' : '44px')};
  margin-right: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ProfileRightWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const ProfileName = styled('div')`
  flex: 1;
  display: flex;
  align-items: center;
  font-weight: 600;
  font-size: ${(props) => (props.big ? '18px' : '14px')};
`;

const ProfileUserName = styled('div')`
  flex: 1;
  color: #999;
  font-weight: 400;
  display: flex;
  align-items: center;
`;

export default class ProfileCard extends PureComponent {
  get src() {
    const { big } = this.props;

    return big
      ? 'https://via.placeholder.com/72x72'
      : 'https://via.placeholder.com/44x44';
  }

  get imageSize() {
    const { big } = this.props;

    return big ? '72px' : '44px';
  }

  handleClick = () => {
    const { id, onClick } = this.props;

    // We pass back the id to the top, to avoid func factory in parent.
    onClick({ id });
  };

  render() {
    const { name, username, big } = this.props;

    return (
      <ProfileCardWrapper onClick={this.handleClick} big={big}>
        <ProfileCardImageWrapper big={big}>
          <Image
            rounded
            src={this.src}
            height={this.imageSize}
            width={this.imageSize}
          />
        </ProfileCardImageWrapper>
        <ProfileRightWrapper>
          <ProfileName big={big}>{name}</ProfileName>
          {username && <ProfileUserName>{username}</ProfileUserName>}
          {big && <ProfileUserName>denichodev@gmail.com</ProfileUserName>}
        </ProfileRightWrapper>
      </ProfileCardWrapper>
    );
  }
}

ProfileCard.propTypes = {
  big: bool,
  id: number.isRequired,
  name: string.isRequired,
  onClick: func,
  username: string
};

ProfileCard.defaultProps = {
  big: false,
  onClick: () => {},
  username: '',
};
