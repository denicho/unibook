import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import Header from '../index';

const homeProps = {
  location: {
    pathname: '/',
  },
  history: {
    push: jest.fn(),
  }
};

const otherProps = {
  location: {
    pathname: '/other',
  },
  history: {
    push: jest.fn(),
    goBack: jest.fn(),
  }
};

const setup = (props) => {
  const utils = render(<Header {...props} />);
  const wrapper = utils.getByTestId('headerWrapper');
  const logo = utils.getByTestId('logo');

  return {
    wrapper,
    logo,
    props,
    ...utils
  };
};

describe('<Header />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup(homeProps);

    expect(tree).toMatchSnapshot();
  });

  test('backButton works', () => {
    const tree = setup(otherProps);
    const backBtn = tree.getByTestId('backBtn');

    fireEvent.click(backBtn);

    expect(otherProps.history.goBack).toHaveBeenCalled();
  });

  test('logo click go to home', () => {
    const tree = setup(otherProps);
    const logo = tree.getByTestId('logo');

    fireEvent.click(logo);

    expect(otherProps.history.push).toHaveBeenCalled();
  })
});
