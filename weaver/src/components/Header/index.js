import React from 'react';
import { object } from 'prop-types';
import styled from 'react-emotion';

import { border } from '../../colors';
import AppContext from '../../Context';
import BackIconSrc from './back.svg';

const HeaderWrapper = styled('div')`
  width: 100%;
  padding-left: 16px;
  padding-right: 16px;
  display: flex;
  justify-content: center;
  border-bottom: 1px solid ${border};
`;

const HeaderLogo = styled('div')`
  font-weight: 600;
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 18px;
`;

const BackButton = styled('div')`
  width: 20%;
  position: absolute;
  left: 16px;
  top: 14px;
`;

const BackIcon = styled('img')`
  width: 16px;
`;

const Header = ({ history, location }) => (
  <AppContext.Consumer>
    {({ headerText }) => (
      <HeaderWrapper data-testid="headerWrapper">
        {location.pathname !== '/' && (
          <BackButton data-testid="backBtn" onClick={() => history.goBack()}>
            <BackIcon src={BackIconSrc} />
          </BackButton>
        )}
        <HeaderLogo
          data-testid="logo"
          onClick={() => location.pathname !== '/' && history.push('/')}>
          {headerText}
        </HeaderLogo>
      </HeaderWrapper>
    )}
  </AppContext.Consumer>
);

Header.propTypes = {
  history: object.isRequired,
  location: object.isRequired
};

export default Header;
