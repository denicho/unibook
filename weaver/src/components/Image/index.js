import React, { PureComponent } from 'react';
import styled from 'react-emotion';
import { string, bool } from 'prop-types';

const ImageShape = styled('img')`
  border-radius: ${props => props.rounded ? '50%' : 0};
  width: ${props => props.width};
  height: ${props => props.height};
`;

export default class Image extends PureComponent {
  render() {
    const { width, height, src, rounded } = this.props;

    return <ImageShape rounded={rounded} width={width} height={height} src={src} />;
  }
}

Image.propTypes = {
  height: string,
  rounded: bool,
  src: string.isRequired,
  width: string,
};

Image.defaultProps = {
  height: 'auto',
  width: 'auto',
  rounded: true,
};
