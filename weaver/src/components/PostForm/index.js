import React, { Component } from 'react';
import { func, string } from 'prop-types';
import styled, { css } from 'react-emotion';

import Tabs from '../Tabs';
import { border, blue } from '../../colors';

const PostFormWrapper = styled('div')`
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 16px;
`;

const TextInput = styled('input')`
  border: none;
  display: block;
  border-bottom: 1px solid ${border};
  width: ${(props) => (props.width ? props.width : 'auto')};
  font-weight: ${(props) => (props.bold ? 600 : 400)};
  padding: 8px;

  &:focus {
    outline: none;
    border-bottom: 1px solid ${blue};
  }
`;

const textAreaClass = css`
  font-family: inherit !important;
  border: none;
  border-bottom: 1px solid ${border};
  padding-left: 8px;
  padding-top: 8px;

  &:focus {
    outline: none;
    border-bottom: 1px solid ${blue};
  }
`;

const activeText = css`
  color: ${blue};
`;

export default class PostForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.defaultTitle,
      body: props.defaultBody
    };
  }

  get tabs() {
    const { actionButtonText } = this.props;

    return [
      {
        id: 1,
        name: 'submit',
        childNode: <span className={activeText}>{actionButtonText}</span>,
        align: 0
      }
    ];
  }

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { title, body } = this.state;

    if (!title || !body) {
      return;
    }

    onSubmit(this.state);
  };

  handleEmailChange = ({ target }) => {
    this.setState({ email: target.value });
  };

  handleTitleChange = ({ target }) => {
    this.setState({ title: target.value });
  };

  handleBodyChange = ({ target }) => {
    this.setState({ body: target.value });
  };

  render() {
    const { title, body } = this.state;

    return (
      <PostFormWrapper>
        <TextInput
          type="text"
          placeholder="Title"
          onChange={this.handleTitleChange}
          width="200px"
          bold
          value={title}
        />
        <textarea
          className={textAreaClass}
          onChange={this.handleBodyChange}
          value={body}
          placeholder="Body"
          rows="5"
          cols="35"
        />
        <Tabs
          tabs={this.tabs}
          activeId={1}
          onClick={this.handleSubmit}
          noBorder
        />
      </PostFormWrapper>
    );
  }
}

PostForm.propTypes = {
  actionButtonText: string,
  defaultBody: string,
  defaultTitle: string,
  onSubmit: func
};

PostForm.defaultProps = {
  onSubmit: () => {},
  defaultBody: '',
  defaultTitle: '',
  actionButtonText: 'Submit'
};
