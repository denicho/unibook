import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import CommentForm from '../index';

const setup = () => {
  const props = {
    actionButtonText: '',
    defaultBody: 'This is a body',
    defaultEmail: 'denichodev@gmail.com',
    defaultTitle: 'Comment title',
    onSubmit: jest.fn(),
  };
  const utils = render(<CommentForm {...props} />);
  const emailInput = utils.getByTestId('emailInput');
  const titleInput = utils.getByTestId('titleInput');
  const bodyInput = utils.getByTestId('bodyInput');
  const submit = utils.getByTestId('tabItem-1');

  return {
    emailInput,
    titleInput,
    bodyInput,
    submit,
    props,
    ...utils
  };
};

describe('<CommentForm />', () => {
  afterEach(cleanup);

  test('match snapshot', () => {
    const tree = setup();

    expect(tree).toMatchSnapshot();
  });

  test('value matches props', () => {
    const { emailInput, titleInput, bodyInput } = setup();

    expect(emailInput.value).toBe('denichodev@gmail.com');
    expect(bodyInput.value).toBe('This is a body');
    expect(titleInput.value).toBe('Comment title');
  })

  test('value change match', () => {
    const { emailInput, titleInput, bodyInput } = setup();

    fireEvent.change(emailInput, {target: {value: 'a'}});
    fireEvent.change(titleInput, {target: {value: 'b'}});
    fireEvent.change(bodyInput, {target: {value: 'c'}});

    expect(emailInput.value).toBe('a');
    expect(titleInput.value).toBe('b');
    expect(bodyInput.value).toBe('c');
  })

  test('submit click triggers fn', () => {
    const { submit, props } = setup();

    fireEvent.click(submit);

    const firstCalls = props.onSubmit.mock.calls[0][0];

    expect(props.onSubmit).toHaveBeenCalled();
    expect(firstCalls.title).toBe(props.defaultTitle);
    expect(firstCalls.email).toBe(props.defaultEmail);
    expect(firstCalls.name).toBe(props.defaultName);
  })
});
