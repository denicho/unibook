import React, { Component } from 'react';
import { func, string } from 'prop-types';
import styled, { css } from 'react-emotion';

import Tabs from '../Tabs';
import { border, blue } from '../../colors';

const CommentWrapper = styled('div')`
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 16px;
`;

const TextInput = styled('input')`
  border: none;
  display: block;
  border-bottom: 1px solid ${border};
  width: ${(props) => (props.width ? props.width : 'auto')};
  font-weight: ${(props) => (props.bold ? 600 : 400)};
  padding: 8px;

  &:focus {
    outline: none;
    border-bottom: 1px solid ${blue};
  }
`;

const textAreaClass = css`
  font-family: inherit !important;
  border: none;
  border-bottom: 1px solid ${border};
  padding-left: 8px;
  padding-top: 8px;

  &:focus {
    outline: none;
    border-bottom: 1px solid ${blue};
  }
`;

const activeText = css`
  color: ${blue};
`;

export default class CommentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.defaultTitle,
      email: props.defaultEmail,
      body: props.defaultBody,
    };
  }

  get tabs() {
    const { actionButtonText } = this.props;

    return [
      {
        id: 1,
        name: 'submit',
        childNode: (
          <span className={activeText}>
            {actionButtonText}
          </span>
        ),
        align: 0
      }
    ];
  }

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { title, email, body } = this.state;

    if (!title || !email || !body) {
      return;
    }

    onSubmit(this.state);
  };

  handleEmailChange = ({ target }) => {
    this.setState({ email: target.value });
  };

  handleTitleChange = ({ target }) => {
    this.setState({ title: target.value });
  };

  handleBodyChange = ({ target }) => {
    this.setState({ body: target.value });
  };

  render() {
    const { email, title, body } = this.state;

    return (
      <CommentWrapper>
        <TextInput
          data-testid="emailInput"
          type="email"
          placeholder="Email"
          onChange={this.handleEmailChange}
          value={email}
        />
        <TextInput
          data-testid="titleInput"
          type="text"
          placeholder="Title"
          onChange={this.handleTitleChange}
          width="200px"
          bold
          value={title}
        />
        <textarea
          data-testid="bodyInput"
          className={textAreaClass}
          onChange={this.handleBodyChange}
          value={body}
          placeholder="Comment"
          rows="5"
          cols="35"
        />
        <Tabs
          testid="submit"
          tabs={this.tabs}
          activeId={1}
          onClick={this.handleSubmit}
          noBorder
        />
      </CommentWrapper>
    );
  }
}

CommentForm.propTypes = {
  actionButtonText: string,
  defaultBody: string,
  defaultEmail: string,
  defaultTitle: string,
  onSubmit: func,
}

CommentForm.defaultProps = {
  onSubmit: () => {},
  defaultBody: '',
  defaultEmail: '',
  defaultTitle: '',
  actionButtonText: 'Submit',
}
