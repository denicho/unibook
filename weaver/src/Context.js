import React from 'react';

const AppContext = React.createContext({
  headerText: '',
  setHeaderText: () => {},
});

export default AppContext;
