import React, { Component } from 'react';
import styled from 'react-emotion';
import { get } from 'lodash';
import { func, object } from 'prop-types';
import { Query } from 'react-apollo';

import PhotoQuery from './queries/Photo.graphql';
import AlbumQuery from './queries/Album.graphql';
import AppContext from '../../Context';

import { muteBlack } from '../../colors';

const BigPhoto = styled('img')`
  width: 100%;
`;

const PhotoTitle = styled('div')`
  font-size: 18px;
  font-weight: 600;
  padding: 8px 16px;
`;

const AlbumTitle = styled('div')`
  padding-left: 16px;
  padding-right: 16px;
  color: ${muteBlack};
`;

class Photo extends Component {
  constructor(props) {
    super(props);

    props.setHeaderText('Photo');
  }

  get photoId() {
    return Number(get(this.props, 'match.params.photoId', 0));
  }

  handleProfileClick = ({ id }) => {
    const { history } = this.props;

    history.push(`/user/${id}`);
  };

  render() {
    return (
      <Query query={PhotoQuery} variables={{ photoId: this.photoId }}>
        {({ data, loading }) => {
          if (loading) {
            return 'Loading...';
          }

          const { photo } = data;

          return (
            <div>
              <BigPhoto src={photo.url} />
              <PhotoTitle>{photo.title}</PhotoTitle>
              <Query query={AlbumQuery} variables={{ albumId: photo.albumId }}>
              {({ data: albumData, loading: albumLoading }) => {
                if (albumLoading) {
                  return null;
                }

                const { album } = albumData;

                return <AlbumTitle>Album: {album.title}</AlbumTitle>;
              }}
              </Query>
            </div>
          );
        }}
      </Query>
    );
  }
}

Photo.propTypes = {
  history: object.isRequired,
  setHeaderText: func.isRequired
};

export default (props) => (
  <AppContext.Consumer>
    {({ setHeaderText }) => <Photo setHeaderText={setHeaderText} {...props} />}
  </AppContext.Consumer>
);
