import React, { Component, Fragment } from 'react';
import { func, object } from 'prop-types';
import { get } from 'lodash';
import { Query } from 'react-apollo';

import PhotosQuery from './queries/Photos.graphql';
import AlbumQuery from './queries/Album.graphql';
import UserQuery from './queries/User.graphql';
import AppContext from '../../Context';
import ProfileCard from '../../components/ProfileCard';
import PhotoGrid from '../../components/PhotoGrid';

class Album extends Component {
  constructor(props) {
    super(props);

    props.setHeaderText('Album');
  }

  get albumId() {
    return Number(get(this.props, 'match.params.albumId', 0));
  }

  handlePhotoClick = ({ id: photoId }) => {
    const { history } = this.props;

    history.push(`/photo/${photoId}`);
  }

  render() {
    return (
      <Query query={AlbumQuery} variables={{ albumId: this.albumId }}>
        {({ data: albumData, loading: albumLoading }) => {
          if (albumLoading) {
            return 'Loading...';
          }

          const { album } = albumData;

          return (
            <Query query={UserQuery} variables={{ userId: album.userId }}>
              {({ data: userData, loading: userLoading }) => {
                if (userLoading) {
                  return 'Loading...';
                }

                const { user } = userData;

                return (
                  <Query
                    query={PhotosQuery}
                    variables={{
                      albumId: this.albumId
                    }}>
                    {({ data: photosData, loading: photosLoading }) => {
                      if (photosLoading) {
                        return 'Loading...';
                      }

                      const { photos } = photosData;

                      return (
                        <Fragment>
                          <ProfileCard
                            big
                            name={user.name}
                            id={user.id}
                            username={user.username}
                            onClick={this.handleProfileClick}
                          />
                          <PhotoGrid photos={photos} onPhotoClick={this.handlePhotoClick} />
                        </Fragment>
                      );
                    }}
                  </Query>
                );
              }}
            </Query>
          );
        }}
      </Query>
    );
  }
}

Album.propTypes = {
  history: object.isRequired,
  setHeaderText: func.isRequired
};

export default (props) => (
  <AppContext.Consumer>
    {({ setHeaderText }) => <Album setHeaderText={setHeaderText} {...props} />}
  </AppContext.Consumer>
);
