import React, { Component, Fragment } from 'react';
import { func, object } from 'prop-types';
import { get } from 'lodash';
import { Query } from 'react-apollo';

import ProfileCard from '../../components/ProfileCard';
import Tabs from '../../components/Tabs';
import PostList from './PostList';
import AlbumList from './AlbumList';
import UserQuery from './queries/User.graphql';
import albumIcon from './assets/album.svg';
import albumIconActive from './assets/album-active.svg';
import postIcon from './assets/post.svg';
import postIconActive from './assets/post-active.svg';
import AppContext from '../../Context';

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 1,
    }

    props.setHeaderText('Profile')
  }

  get userId() {
    return Number(get(this.props, 'match.params.userId', 0));
  }

  get tabs() {
    return [
      {
        id: 1,
        name: 'post',
        icon: postIcon,
        activeIcon: postIconActive
      },
      {
        id: 2,
        name: 'album',
        icon: albumIcon,
        activeIcon: albumIconActive
      },
    ];
  }

  handleTabClick = ({ id }) => {
    this.setState({ activeTab: id });
  }

  handlePostDetailClick = ({ id: postId }) => {
    const { history } = this.props;

    history.push(`/post/${postId}`);
  }

  handleAlbumDetailClick = ({ id: albumId }) => {
    const { history } = this.props;

    history.push(`/album/${albumId}`);
  }


  render() {
    const { activeTab } = this.state;

    return (
      <Query
        query={UserQuery}
        variables={{
          userId: this.userId
        }}>
        {({ data, loading }) => {
          if (loading) {
            return 'Loading...';
          }

          const { user } = data;

          return (
            <Fragment>
              <ProfileCard
                big
                name={user.name}
                id={user.id}
                username={user.username}
                onClick={this.handleProfileClick}
              />
              <Tabs tabs={this.tabs} activeId={activeTab} onClick={this.handleTabClick} />
              {activeTab === 1 && <PostList user={user} onDetailClick={this.handlePostDetailClick} />}
              {activeTab === 2 && <AlbumList user={user} onDetailClick={this.handleAlbumDetailClick} />}
            </Fragment>
          );
        }}
      </Query>
    );
  }
}

User.propTypes = {
  history: object.isRequired,
  setHeaderText: func.isRequired,
}

export default (props) => (
  <AppContext.Consumer>
    {({ setHeaderText }) => <User setHeaderText={setHeaderText} {...props} />}
  </AppContext.Consumer>
);
