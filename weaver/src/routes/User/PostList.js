import React, { Component, Fragment } from 'react';
import { get } from 'lodash';
import { css } from 'react-emotion';
import { shape, string, number, func } from 'prop-types';
import { Query, Mutation } from 'react-apollo';

import Tabs from '../../components/Tabs';
import PostForm from '../../components/PostForm';
import PostItem from './PostItem';
import PostByUserQuery from './queries/PostByUser.graphql';
import AddPostMutation from './queries/AddPost.graphql';
import EditPostMutation from './queries/EditPost.graphql';
import DeletePostMutation from './queries/DeletePost.graphql';
import { blue } from '../../colors';

const activeText = css`
  color: ${blue};
`;

const leftRightPadding = css`
  padding-left: 16px;
  padding-right: 16px;
`;

export default class PostList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showForm: false,
      editId: 0
    };
  }

  get tabs() {
    return [
      {
        id: 1,
        name: 'AddPost',
        childNode: <span className={activeText}>Add Post</span>,
        align: 1
      }
    ];
  }

  get userId() {
    return Number(get(this.props, 'match.params.userId', 0));
  }

  toggleForm = () => {
    this.setState((prevState) => ({
      showForm: !prevState.showForm
    }));
  };

  renderForm = (posts) => {
    const { editId } = this.state;
    const [target] = posts.filter((post) => post.id === editId);

    if (editId === 0) {
      return this.renderAddPostForm();
    }

    return this.renderEditForm(target);
  };

  renderEditForm = (target) => {
    const { user } = this.props;

    return (
      <Mutation mutation={EditPostMutation}>
        {(addPost) => (
          <PostForm
            actionButtonText="Edit Post"
            defaultTitle={target.title}
            defaultBody={target.body}
            onSubmit={({ title, body }) => {
              addPost({
                variables: { id: target.id, userId: user.id, title, body },
                optimisticResponse: {
                  editPost: {
                    __typename: 'PostType',
                    id: target.id,
                    userId: user.id,
                    title,
                    body
                  }
                },
                update: (proxy, { data }) => {
                  const queryOpts = {
                    query: PostByUserQuery,
                    variables: { userId: user.id }
                  };
                  const postByUserData = proxy.readQuery(queryOpts);
                  const newPost = data.editPost;

                  postByUserData.posts.map((post) => {
                    if (post.id === newPost.id) {
                      return newPost;
                    }

                    return post;
                  });

                  proxy.writeQuery({ ...queryOpts, data: postByUserData });
                }
              });

              this.setState({ showForm: false });
            }}
          />
        )}
      </Mutation>
    );
  };

  handlePostEdit = ({ id: postId }) => {
    this.setState({
      showForm: true,
      editId: postId
    });
  };

  renderAddPostForm = () => {
    const { user } = this.props;

    return (
      <Mutation mutation={AddPostMutation}>
        {(addPost) => (
          <PostForm
            actionButtonText="Add Post"
            onSubmit={({ title, body }) => {
              addPost({
                variables: { userId: user.id, title, body },
                optimisticResponse: {
                  addPost: {
                    __typename: 'PostType',
                    id: 101,
                    userId: user.id,
                    title,
                    body
                  }
                },
                update: (proxy, { data }) => {
                  const queryOpts = {
                    query: PostByUserQuery,
                    variables: { userId: user.id }
                  };
                  const postByUserData = proxy.readQuery(queryOpts);
                  const newPost = data.addPost;

                  postByUserData.posts.unshift(newPost);

                  proxy.writeQuery({ ...queryOpts, data: postByUserData });
                }
              });

              this.setState({ showForm: false });
            }}
          />
        )}
      </Mutation>
    );
  };

  render() {
    const { user, onDetailClick } = this.props;
    const { showForm } = this.state;

    return (
      <div>
        <div className={leftRightPadding}>
          <Tabs
            tabs={this.tabs}
            activeId={1}
            onClick={this.toggleForm}
            noBorder
          />
        </div>
        <Query query={PostByUserQuery} variables={{ userId: user.id }}>
          {({ data, loading }) => {
            if (loading) {
              return 'Loading...';
            }

            const { posts } = data;

            const postListComponent = posts.map((post) => (
              <Mutation mutation={DeletePostMutation} key={post.id}>
                {(deletePost) => (
                  <PostItem
                    onDetailClick={onDetailClick}
                    user={user}
                    id={post.id}
                    title={post.title}
                    body={post.body}
                    onEdit={this.handlePostEdit}
                    onDelete={() => {
                      deletePost({
                        variables: { id: post.id },
                        optimisticResponse: {
                          deletePost: {
                            __typename: 'PostType',
                            id: post.id
                          }
                        },
                        update: (proxy) => {
                          const queryOpts = {
                            query: PostByUserQuery,
                            variables: { userId: user.id }
                          };
                          const postByUserData = proxy.readQuery(queryOpts);
                          const newPosts = postByUserData.posts.filter(
                            (p) => p.id !== post.id
                          );

                          proxy.writeQuery({
                            ...queryOpts,
                            data: {
                              ...postByUserData,
                              posts: newPosts
                            }
                          });
                        }
                      });
                    }}
                  />
                )}
              </Mutation>
            ));

            return (
              <Fragment>
                {showForm && this.renderForm(posts)}
                {postListComponent}
              </Fragment>
            );
          }}
        </Query>
      </div>
    );
  }
}

PostList.propTypes = {
  onDetailClick: func,
  user: shape({
    name: string,
    id: number,
    username: string
  }).isRequired
};

PostList.defaultProps = {
  onDetailClick: () => {}
};
