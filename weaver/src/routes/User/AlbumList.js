import React, { Component } from 'react';
import { shape, string, number, func } from 'prop-types';
import { Query } from 'react-apollo';

import Card from '../../components/Entry/Card';
import AlbumPost from '../../components/Entry/AlbumPost';
import AlbumByUserQuery from './queries/AlbumByUser.graphql';

export default class AlbumList extends Component {
  render() {
    const { user, onDetailClick } = this.props;

    return (
      <Query query={AlbumByUserQuery} variables={{ userId: user.id }}>
        {({ data, loading }) => {
          if (loading) {
            return 'Loading...';
          }

          const { albums } = data;

          return albums.map((album) => (
            <Card user={user} key={album.id} id={album.id} onDetailClick={onDetailClick}>
              <AlbumPost title={album.title} />
            </Card>
          ));
        }}
      </Query>
    );
  }
}

AlbumList.propTypes = {
  onDetailClick: func,
  user: shape({
    name: string,
    id: number,
    username: string
  }).isRequired
};

AlbumList.defaultProps = {
  onDetailClick: () => {},
}
