import React, { PureComponent } from 'react';
import { css } from 'react-emotion';
import { string, number, shape, func } from 'prop-types';

import Tabs from '../../components/Tabs';
import TextPost from '../../components/Entry/TextPost';
import Card from '../../components/Entry/Card';
import { blue } from '../../colors';

const editText = css`
  color: ${blue};
`;

const deleteText = css`
  color: red;
`;

export default class PostItem extends PureComponent {
  get tabs() {
    return [
      {
        id: 1,
        name: 'edit',
        childNode: (
          <span className={editText} onClick={this.handleEditClick}>
            Edit
          </span>
        ),
        align: 0
      },
      {
        id: 2,
        name: 'delete',
        childNode: (
          <span className={deleteText} onClick={this.handleDeleteClick}>
            Delete
          </span>
        ),
        align: 0
      }
    ];
  }

  handleEditClick = () => {
    const { onEdit, id } = this.props;

    onEdit({ id });
  }

  handleDeleteClick = () => {
    const { onDelete, id } = this.props;

    onDelete({ id });
  }

  render() {
    const { user, id, title, body, onDetailClick } = this.props;

    return (
      <div>
        <Card
          noBorder
          user={user}
          key={id}
          onDetailClick={onDetailClick}
          id={id}>
          <TextPost title={title} body={body} />
        </Card>
        <Tabs tabs={this.tabs} activeId={1} />
      </div>
    );
  }
}

PostItem.propTypes = {
  body: string.isRequired,
  id: number.isRequired,
  onDelete: func,
  onDetailClick: func,
  onEdit: func,
  title: string.isRequired,
  user: shape({
    name: string,
    id: number,
    username: string
  }).isRequired
};

PostItem.defaultProps = {
  onDetailClick: () => {},
  onEdit: () => {},
  onDelete: () => {},
};
