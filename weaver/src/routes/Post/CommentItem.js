import React, { Component } from 'react';
import { string, number, func } from 'prop-types';
import styled, { css } from 'react-emotion';

import Tabs from '../../components/Tabs';

import { muteBlack, blue } from '../../colors';

const CommentItemWrapper = styled('div')`
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 16px;
`;

const CommentItemEmail = styled('div')`
  color: ${muteBlack};
`;

const CommentItemName = styled('div')`
  padding-top: 4px;
  font-weight: 600;
`;

const CommentItemBody = styled('div')`
  padding-top: 4px;
`;

const editText = css`
  color: ${blue};
`;

const deleteText = css`
  color: red;
`;

class CommentItem extends Component {
  get tabs() {
    return [
      {
        id: 1,
        name: 'edit',
        childNode: <span className={editText} onClick={this.handleEditClick}>Edit</span>,
        align: 0
      },
      {
        id: 2,
        name: 'delete',
        childNode: <span className={deleteText} onClick={this.handleDeleteClick}>Delete</span>,
        align: 0
      }
    ];
  }

  handleEditClick = () => {
    const { id, onEdit } = this.props;

    onEdit({id});
  };

  handleDeleteClick = () => {
    const { id, onDelete } = this.props;

    onDelete({id});
  };

  render() {
    const { name, email, body } = this.props;

    return (
      <CommentItemWrapper>
        <CommentItemEmail>{email}</CommentItemEmail>
        <CommentItemName>{name}</CommentItemName>
        <CommentItemBody>{body}</CommentItemBody>
        <Tabs tabs={this.tabs} activeId={1} noBorder />
      </CommentItemWrapper>
    );
  }
}

CommentItem.propTypes = {
  body: string,
  email: string,
  id: number.isRequired,
  name: string,
  onDelete: func,
  onEdit: func
};

CommentItem.defaultProps = {
  body: '',
  email: '',
  name: '',
  onDelete: () => {},
  onEdit: () => {}
};

export default CommentItem;
