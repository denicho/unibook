import React, { Component, Fragment } from 'react';
import styled from 'react-emotion';
import { func } from 'prop-types';
import { get } from 'lodash';
import { Query, Mutation } from 'react-apollo';

import ProfileCard from '../../components/ProfileCard';
import CommentForm from '../../components/CommentForm';
import TextPost from '../../components/Entry/TextPost';
import CommentItem from './CommentItem';
import AppContext from '../../Context';
import PostPageQuery from './queries/PostPage.graphql';
import AddCommentMutation from './queries/AddComment.graphql';
import EditCommentMutation from './queries/EditComment.graphql';
import DeleteCommentMutation from './queries/DeleteComment.graphql';
import UserQuery from './queries/User.graphql';

import { blue } from '../../colors';

const CommentSeparator = styled('div')`
  font-weight: 600;
  font-size: 14px;
  padding: 16px 16px 16px 16px;
  display: flex;
  justify-content: space-between;
`;

const AddComment = styled('div')`
  text-align: right;
  width: 20%;
  color: ${blue};
`;

class Post extends Component {
  constructor(props) {
    super(props);

    props.setHeaderText('Post');

    this.state = {
      showForm: false,
      editId: 0
    };
  }

  get postId() {
    return Number(get(this.props, 'match.params.postId', 0));
  }

  toggleForm = () => {
    this.setState((prevState) => ({
      showForm: !prevState.showForm,
      editId: 0
    }));
  };

  renderForm = (comments) => {
    const { editId } = this.state;

    if (editId === 0) {
      return this.renderAddCommentForm();
    }

    return this.renderEditForm(comments);
  };

  renderEditForm = (comments) => {
    const { editId } = this.state;
    const [target] = comments.filter((comment) => comment.id === editId);

    return this.renderEditCommentForm(target);
  };

  renderEditCommentForm = (target) => (
    <Mutation mutation={EditCommentMutation}>
      {(editComment) => (
        <CommentForm
          actionButtonText="Edit Comment"
          onSubmit={({ title: name, email, body }) => {
            editComment({
              variables: {
                postId: this.postId,
                id: target.id,
                name,
                email,
                body
              },
              optimisticResponse: {
                editComment: {
                  __typename: 'CommentType',
                  id: target.id,
                  postId: this.postId,
                  name,
                  body,
                  email
                }
              },
              update: (proxy, { data }) => {
                const queryOpts = {
                  query: PostPageQuery,
                  variables: { postId: this.postId }
                };
                const postPageData = proxy.readQuery(queryOpts);
                const newComment = data.editComment;

                postPageData.comments.map((comment) => {
                  if (comment.id === newComment.id) {
                    return newComment;
                  }

                  return comment;
                });

                proxy.writeQuery({ ...queryOpts, data: postPageData });
              }
            });
            this.setState({ showForm: false, editId: 0 });
          }}
          defaultEmail={target.email}
          defaultTitle={target.name}
          defaultBody={target.body}
        />
      )}
    </Mutation>
  );

  renderAddCommentForm = () => (
    <Mutation mutation={AddCommentMutation}>
      {(addComment) => (
        <CommentForm
          actionButtonText="Add Comment"
          onSubmit={({ title: name, email, body }) => {
            addComment({
              variables: { postId: this.postId, name, email, body },
              optimisticResponse: {
                addComment: {
                  __typename: 'CommentType',
                  id: 501,
                  postId: this.postId,
                  body,
                  name,
                  email
                }
              },
              update: (proxy, { data }) => {
                const queryOpts = {
                  query: PostPageQuery,
                  variables: { postId: this.postId }
                };
                const postPageData = proxy.readQuery(queryOpts);
                const newComment = data.addComment;

                postPageData.comments.unshift(newComment);

                proxy.writeQuery({ ...queryOpts, data: postPageData });
              }
            });

            this.setState({ showForm: false });
          }}
        />
      )}
    </Mutation>
  );

  handleCommentEdit = ({ id: commentId }) => {
    this.setState({
      showForm: true,
      editId: commentId
    });
  };

  render() {
    const { showForm } = this.state;

    return (
      <Query
        query={PostPageQuery}
        variables={{
          postId: this.postId
        }}>
        {({ data, loading }) => {
          if (loading) {
            return 'Loading...';
          }

          const { post, comments } = data;

          if (!post.body) {
            return 'Not found...';
          }

          const Profile = (
            <Query query={UserQuery} variables={{ userId: post.userId }}>
              {({ data: userData, loading: userLoading }) => {
                if (userLoading) {
                  return 'Loading...';
                }

                const { user } = userData;

                return (
                  <ProfileCard
                    name={user.name}
                    id={user.id}
                    username={user.username}
                    onClick={this.handleProfileClick}
                  />
                );
              }}
            </Query>
          );

          return (
            <Fragment>
              {Profile}
              <TextPost title={post.title} body={post.body} />
              <CommentSeparator>
                Comments
                <AddComment onClick={this.toggleForm}>
                  {showForm ? 'Close' : 'Add'}
                </AddComment>
              </CommentSeparator>
              {showForm && this.renderForm(comments)}
              {comments.map((comment) => (
                <Mutation mutation={DeleteCommentMutation} key={comment.id}>
                  {(deleteComment) => (
                    <CommentItem
                      id={comment.id}
                      onEdit={this.handleCommentEdit}
                      onDelete={() => {
                        deleteComment({
                          variables: { id: comment.id },
                          optimisticResponse: {
                            deleteComment: {
                              __typename: 'CommentType',
                              id: comment.id
                            }
                          },
                          update: (proxy) => {
                            const queryOpts = {
                              query: PostPageQuery,
                              variables: { postId: this.postId }
                            };
                            const postPageData = proxy.readQuery(queryOpts);

                            const newComments = postPageData.comments.filter(
                              (c) => c.id !== comment.id
                            );

                            proxy.writeQuery({
                              ...queryOpts,
                              data: {
                                ...postPageData,
                                comments: newComments
                              }
                            });
                          }
                        });
                      }}
                      name={comment.name}
                      email={comment.email}
                      body={comment.body}
                    />
                  )}
                </Mutation>
              ))}
            </Fragment>
          );
        }}
      </Query>
    );
  }
}

Post.propTypes = {
  setHeaderText: func.isRequired
};

export default (props) => (
  <AppContext.Consumer>
    {({ setHeaderText }) => <Post setHeaderText={setHeaderText} {...props} />}
  </AppContext.Consumer>
);
