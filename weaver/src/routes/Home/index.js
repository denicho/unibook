import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { Query } from 'react-apollo';

import UsersQuery from './queries/Users.graphql';
import ProfileCard from '../../components/ProfileCard';

import AppContext from '../../Context';

class Home extends Component {
  constructor(props) {
    super(props);

    props.setHeaderText('u n i b o o k');
  }

  handleProfileClick = ({ id }) => {
    const { history } = this.props;

    history.push(`/user/${id}`);
  };

  render() {
    return (
      <Query query={UsersQuery}>
        {({ data, loading }) => {
          const { users } = data;

          if (loading) {
            // to render loading
            return <span>Loading...</span>;
          }

          if (!Array.isArray(users) || !users.length) {
            // to render error
            return <span>Error in getting users data</span>;
          }

          return users.map((user) => (
            <ProfileCard
              key={user.id}
              name={user.name}
              id={user.id}
              username={user.username}
              onClick={this.handleProfileClick}
            />
          ));
        }}
      </Query>
    );
  }
}

Home.propTypes = {
  history: object.isRequired,
  setHeaderText: func.isRequired,
}

export default (props) => (
  <AppContext.Consumer>
    {({ setHeaderText }) => <Home setHeaderText={setHeaderText} {...props} />}
  </AppContext.Consumer>
);
