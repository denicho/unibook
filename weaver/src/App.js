import React, { Component, Fragment } from 'react';
import styled from 'react-emotion';
import { object } from 'prop-types';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import AppContext from './Context';
import Header from './components/Header';
import Home from './routes/Home';
import Album from './routes/Album';
import User from './routes/User';
import Post from './routes/Post';
import Photo from './routes/Photo';

const GlobalWrapper = styled('div')`
  width: 100%;
  min-height: 100vh;
`;

class App extends Component {
  constructor(props) {
    super(props);

    // wrong eslint because we use this as a context
    this.state = {
      // eslint-disable-next-line
      headerText: '',
      // eslint-disable-next-line
      setHeaderText: this.setHeaderText,
    }
  }

  setHeaderText = (text) => {
    // eslint-disable-next-line
    this.setState({ headerText: text })
  }

  render() {
    const { client } = this.props;

    return (
      <AppContext.Provider value={this.state}>
        <ApolloProvider client={client}>
          <GlobalWrapper>
            <Router>
              <Fragment>
                <Route component={Header} />
                <Route exact path="/" component={Home} />
                <Route path="/user/:userId" component={User} />
                <Route path="/post/:postId" component={Post} />
                <Route path="/album/:albumId" component={Album} />
                <Route path="/photo/:photoId" component={Photo} />
              </Fragment>
            </Router>
          </GlobalWrapper>
        </ApolloProvider>
      </AppContext.Provider>
    );
  }
}

App.propTypes = {
  client: object.isRequired
};

export default App;
