import { GraphQLNonNull, GraphQLInt, GraphQLString } from 'graphql';

import PostType from '../types/PostType';
import { addPost, editPost, deletePost } from '../models/posts';

// eslint-disable-next-line
export const addPostMutation = {
  type: PostType,
  args: {
    userId: { type: new GraphQLNonNull(GraphQLInt) },
    title: { type: new GraphQLNonNull(GraphQLString) },
    body: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: addPost,
}

export const editPostMutation = {
  type: PostType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLInt) },
    userId: { type: new GraphQLNonNull(GraphQLInt) },
    title: { type: new GraphQLNonNull(GraphQLString) },
    body: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: editPost,
}

export const deletePostMutation = {
  type: PostType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: deletePost,
}
