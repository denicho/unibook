import { GraphQLNonNull, GraphQLInt, GraphQLString } from 'graphql';

import CommentType from '../types/CommentType';
import { addComment, editComment, deleteComment } from '../models/comments';

export const addCommentMutation = {
  type: CommentType,
  args: {
    postId: { type: new GraphQLNonNull(GraphQLInt) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    body: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: addComment,
}

export const editCommentMutation = {
  type: CommentType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLInt) },
    postId: { type: new GraphQLNonNull(GraphQLInt) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    body: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: editComment,
}

export const deleteCommentMutation = {
  type: CommentType,
  args: {
    id: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: deleteComment,
}

