import {
  addCommentMutation,
  editCommentMutation,
  deleteCommentMutation
} from './comments';
import {
  addPostMutation,
  editPostMutation,
  deletePostMutation
} from './posts';

const mutations = {
  addComment: addCommentMutation,
  editComment: editCommentMutation,
  deleteComment: deleteCommentMutation,
  addPost: addPostMutation,
  editPost: editPostMutation,
  deletePost: deletePostMutation,
};

export default mutations;
