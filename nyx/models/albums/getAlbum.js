import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function getAlbum(root, args) {
  const defaultResponse = [];

  return get(`https://jsonplaceholder.typicode.com/albums/${args.albumId}`)
    .then(handleResponse('Get Album', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Album'));
}
