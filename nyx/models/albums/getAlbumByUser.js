import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && Array.isArray(response);

export default function getAlbumByUser(root, args) {
  const defaultResponse = [];

  return get(`https://jsonplaceholder.typicode.com/users/${args.userId}/albums`)
    .then(handleResponse('Get Album by User', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Album by User'));
}
