import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && Array.isArray(response);

export default function getPostByUser(root, args) {
  const defaultResponse = [];

  return get(`https://jsonplaceholder.typicode.com/users/${args.userId}/posts`)
    .then(handleResponse('Get Post by User', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Post by User'));
}
