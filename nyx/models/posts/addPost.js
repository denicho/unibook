import post from '../../helpers/post';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function addPost(root, args) {
  const defaultResponse = [];

  const body = {
    userId: args.userId,
    title: args.title,
    body: args.body
  };

  return post(
    `https://jsonplaceholder.typicode.com/posts`,
    {},
    {
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    }
  )
    .then(
      handleResponse('Add Post', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Add Post'));
}
