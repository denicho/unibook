import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function getPost(root, args) {
  const defaultResponse = {};

  return get(`https://jsonplaceholder.typicode.com/posts/${args.postId}`)
    .then(handleResponse('Get Post', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Post'));
}
