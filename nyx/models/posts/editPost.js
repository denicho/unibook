import put from '../../helpers/put';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function editPost(root, args) {
  const defaultResponse = [];

  const body = {
    title: args.title,
    body: args.body,
    userId: args.userId,
  };

  return put(
    `https://jsonplaceholder.typicode.com/posts/${args.id}`,
    {},
    {
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    }
  )
    .then(
      handleResponse('Edit Post', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Edit Post'));
}
