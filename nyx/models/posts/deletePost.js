import httpDelete from '../../helpers/delete';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function deletePost(root, args) {
  const defaultResponse = [];

  return httpDelete(
    `https://jsonplaceholder.typicode.com/posts/${args.id}`,
  )
    .then(
      handleResponse('Delete Post', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Delete Post'));
}
