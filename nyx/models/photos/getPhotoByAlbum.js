import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && Array.isArray(response);

export default function getPhotoByAlbum(root, args) {
  const defaultResponse = [];

  return get(`https://jsonplaceholder.typicode.com/albums/${args.albumId}/photos`)
    .then(handleResponse('Get Photo by Album', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Photo by Album'));
}
