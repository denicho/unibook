import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function getPhoto(root, args) {
  const defaultResponse = {};

  return get(`https://jsonplaceholder.typicode.com/photos/${args.photoId}`)
    .then(handleResponse('Get Photo', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Photo'));
}
