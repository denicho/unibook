import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const url = 'https://jsonplaceholder.typicode.com/users';

const validator = (response) => response && Array.isArray(response);

export default function getUsers() {
  const defaultResponse = [];

  return get(url)
    .then(handleResponse('Get All Users', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get All Users'));
}
