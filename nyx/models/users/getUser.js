import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function getUsers(root, args) {
  const defaultResponse = {};

  return get(`https://jsonplaceholder.typicode.com/users/${args.userId}`)
    .then(handleResponse('Get Single User', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Single User'));
}
