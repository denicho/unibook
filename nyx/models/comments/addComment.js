import post from '../../helpers/post';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function addComment(root, args) {
  const defaultResponse = [];

  const body = {
    postId: args.postId,
    name: args.name,
    email: args.email,
    body: args.body
  };

  return post(
    `https://jsonplaceholder.typicode.com/comments`,
    {},
    {
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    }
  )
    .then(
      handleResponse('Add Comment', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Add Comment'));
}
