import put from '../../helpers/put';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function editComment(root, args) {
  const defaultResponse = [];

  const body = {
    name: args.name,
    email: args.email,
    body: args.body,
    postId: args.postId,
  };

  return put(
    `https://jsonplaceholder.typicode.com/comments/${args.id}`,
    {},
    {
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    }
  )
    .then(
      handleResponse('Edit Comment', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Edit Comment'));
}
