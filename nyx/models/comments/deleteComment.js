import httpDelete from '../../helpers/delete';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && typeof response === 'object';

export default function deleteComment(root, args) {
  const defaultResponse = [];

  return httpDelete(
    `https://jsonplaceholder.typicode.com/comments/${args.id}`,
  )
    .then(
      handleResponse('Delete Comment', defaultResponse, { validator, raw: true })
    )
    .catch(handleError('Delete Comment'));
}
