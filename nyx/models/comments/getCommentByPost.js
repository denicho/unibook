import get from '../../helpers/get';
import handleResponse from '../../helpers/handleResponse';
import handleError from '../../helpers/handleError';

const validator = (response) => response && Array.isArray(response);

export default function getCommentByPost(root, args) {
  const defaultResponse = [];

  return get(`https://jsonplaceholder.typicode.com/posts/${args.postId}/comments`)
    .then(handleResponse('Get Comment By Post', defaultResponse, { validator, raw: true }))
    .catch(handleError('Get Comment By Post'));
}
