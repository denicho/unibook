import URL from 'url';

import formatGetUrl from './formatGetUrl';
import safeRequest from './safeRequest';

const post = (url, queries, additionalOptions = {}) => {
  const endpoint = formatGetUrl(URL.parse(url), queries);
  const opts = {
    method: 'POST',
    timeout: 2000,
    ...additionalOptions
  };

  return safeRequest(endpoint, opts);
};

export default post;
