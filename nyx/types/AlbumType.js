import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const AlbumType = new GraphQLObjectType({
  name: 'AlbumType',
  fields: {
    id: {
      type: GraphQLInt
    },
    userId: {
      type: GraphQLInt
    },
    title: {
      type: GraphQLString
    },
  }
});

export default AlbumType;
