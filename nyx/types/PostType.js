import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const PostType = new GraphQLObjectType({
  name: 'PostType',
  fields: {
    id: {
      type: GraphQLInt
    },
    userId: {
      type: GraphQLInt
    },
    title: {
      type: GraphQLString
    },
    body: {
      type: GraphQLString
    },
  }
});

export default PostType;
