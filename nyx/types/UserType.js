import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

export const GeoType = new GraphQLObjectType({
  name: 'GeoType',
  fields: {
    lat: {
      type: GraphQLString
    },
    lng: {
      type: GraphQLString
    }
  }
});

export const AddressType = new GraphQLObjectType({
  name: 'AddressType',
  fields: {
    street: {
      type: GraphQLString
    },
    suite: {
      type: GraphQLString
    },
    city: {
      type: GraphQLString
    },
    zipcode: {
      type: GraphQLString
    },
    geo: {
      type: GeoType,
    },
  }
});

export const CompanyType = new GraphQLObjectType({
  name: 'CompanyType',
  fields: {
    name: {
      type: GraphQLString
    },
    catchPhrase: {
      type: GraphQLString
    },
    bs: {
      type: GraphQLString
    }
  }
});

const UserType = new GraphQLObjectType({
  name: 'UserType',
  fields: {
    id: {
      type: GraphQLInt
    },
    name: {
      type: GraphQLString
    },
    username: {
      type: GraphQLString
    },
    email: {
      type: GraphQLString
    },
    address: {
      type: AddressType
    },
    phone: {
      type: GraphQLString
    },
    website: {
      type: GraphQLString
    },
    company: {
      type: CompanyType
    }
  }
});

export default UserType;
