import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const CommentType = new GraphQLObjectType({
  name: 'CommentType',
  fields: {
    id: {
      type: GraphQLInt
    },
    postId: {
      type: GraphQLInt
    },
    name: {
      type: GraphQLString
    },
    email: {
      type: GraphQLString
    },
    body: {
      type: GraphQLString
    },
  }
});

export default CommentType;
