import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const PhotoType = new GraphQLObjectType({
  name: 'PhotoType',
  fields: {
    id: {
      type: GraphQLInt
    },
    albumId: {
      type: GraphQLInt
    },
    title: {
      type: GraphQLString
    },
    url: {
      type: GraphQLString
    },
    thumbnailUrl: {
      type: GraphQLString
    },
  }
});

export default PhotoType;
