import { GraphQLList, GraphQLNonNull, GraphQLInt } from 'graphql';

import CommentType from '../types/CommentType';
import { getCommentByPost } from '../models/comments';

// eslint-disable-next-line
export const commentsByPostQuery = {
  type: new GraphQLList(CommentType),
  args: {
    postId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getCommentByPost,
}
