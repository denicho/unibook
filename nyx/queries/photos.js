import { GraphQLList, GraphQLNonNull, GraphQLInt } from 'graphql';

import PhotoType from '../types/PhotoType';
import { getPhotoByAlbum, getPhoto } from '../models/photos';

// eslint-disable-next-line
export const photosByAlbumQuery = {
  type: new GraphQLList(PhotoType),
  args: {
    albumId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getPhotoByAlbum,
}

export const singlePhotoQuery = {
  type: PhotoType,
  args: {
    photoId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getPhoto,
}
