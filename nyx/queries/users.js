import { GraphQLList, GraphQLInt, GraphQLNonNull } from 'graphql';

import UserType from '../types/UserType';
import { getAllUsers, getUser } from '../models/users';

export const allUsersQuery = {
  type: new GraphQLList(UserType),
  resolve: getAllUsers,
}

export const singleUserQuery = {
  type: UserType,
  args: {
    userId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getUser
}
