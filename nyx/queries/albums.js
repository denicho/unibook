import { GraphQLList, GraphQLNonNull, GraphQLInt } from 'graphql';

import AlbumType from '../types/AlbumType';
import { getAlbumByUser, getAlbum } from '../models/albums';

// eslint-disable-next-line
export const albumsByUserQuery = {
  type: new GraphQLList(AlbumType),
  args: {
    userId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getAlbumByUser,
}

export const singleAlbumQuery = {
  type: AlbumType,
  args: {
    albumId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getAlbum,
}
