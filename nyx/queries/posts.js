import { GraphQLList, GraphQLNonNull, GraphQLInt } from 'graphql';

import PostType from '../types/PostType';
import { getPostByUser, getPost } from '../models/posts';

// eslint-disable-next-line
export const postsByUserQuery = {
  type: new GraphQLList(PostType),
  args: {
    userId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getPostByUser,
}

export const singlePostQuery = {
  type: PostType,
  args: {
    postId: { type: new GraphQLNonNull(GraphQLInt) },
  },
  resolve: getPost,
}
