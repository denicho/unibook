import { allUsersQuery, singleUserQuery } from './users';
import { postsByUserQuery, singlePostQuery } from './posts';
import { albumsByUserQuery, singleAlbumQuery } from './albums';
import { commentsByPostQuery } from './comments';
import { photosByAlbumQuery, singlePhotoQuery } from './photos';

const queries = {
  users: allUsersQuery,
  user: singleUserQuery,
  posts: postsByUserQuery,
  post: singlePostQuery,
  albums: albumsByUserQuery,
  album: singleAlbumQuery,
  comments: commentsByPostQuery,
  photos: photosByAlbumQuery,
  photo: singlePhotoQuery,
};

export default queries;
